Shader "Swanit/UV-cutoff" 
{
    Properties 
    {
       _Shininess ("Shininess", Range (0.03, 1)) = 0.078125
        _MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
        _Tex2("Just Add Again",2D)="white"{}
        _Factor("Cut Off",Range(0,1))=0.2
    }
    SubShader 
    {
        Tags { "RenderType"="Opaque" "IgnoreProjector"="true" "Queue"="Geometry"}
        Cull Off
        LOD 250
    
        CGPROGRAM
        #pragma surface surf MobileBlinnPhong exclude_path:prepass nolightmap noforwardadd halfasview interpolateview
        
        float _Factor;
        
        inline fixed4 LightingMobileBlinnPhong (SurfaceOutput s, fixed3 lightDir, fixed3 halfDir, fixed atten)
        {
            fixed diff = max (0, dot (s.Normal, lightDir));
            fixed nh = max (0, dot (s.Normal, halfDir));
            fixed spec = pow (nh, s.Specular*128) * s.Gloss;
        
            fixed4 c;
            c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec) * atten;
            UNITY_OPAQUE_ALPHA(c.a);
            return c;
        }
        
        sampler2D _MainTex;
        sampler2D _Tex2;
        half _Shininess;
        
        
        struct Input 
        {
            float2 uv_MainTex;
            float2 uv2_Tex2;
        };
        
        void surf (Input IN, inout SurfaceOutput o) 
        {
            fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
            clip((1-IN.uv2_Tex2.y) - (1 -_Factor));
            o.Albedo = tex.rgb;
            o.Gloss = tex.a;
            o.Alpha = tex.a;
            o.Specular = _Shininess;
        }
        ENDCG
    }   

    FallBack "Mobile/VertexLit"
}
