﻿using Sirenix.OdinInspector;
using Sourav.Engine.Core.ControllerRelated;
using Sourav.Engine.Core.NotificationRelated;
using Sourav.Engine.Editable.NotificationRelated;
using UnityEngine;

namespace _Idle3DPrinter._Scripts.TickerRelated
{
    public class Ticker : Controller
    {
        [ReadOnly][SerializeField]private float currentSecondFraction;
        
        public override void OnNotificationReceived(Notification notification, NotificationParam param = null)
        {
            switch (notification)
            {
                case Notification.Update:
                    HandleUpdatePassed();
                    break;
            }
        }

        private void HandleUpdatePassed()
        {
            currentSecondFraction += Time.unscaledDeltaTime;
            if (currentSecondFraction > 1.0f)
            {
                currentSecondFraction = 0.0f;
                App.GetNotificationCenter().Notify(Notification.SecondTick);
            }
        }
    }
}
