﻿using Sourav.Engine.Core.ControllerRelated;
using Sourav.Engine.Core.NotificationRelated;
using Sourav.Engine.Editable.NotificationRelated;
using UnityEngine;

namespace _Idle3DPrinter._Scripts.GameplayRelated.Controllers
{
    public class MainController : Controller
    {
        // Start is called before the first frame update
        void Start()
        {
            LoadGame();
        }

        private void LoadGame()
        {
            App.GetNotificationCenter().Notify(Notification.LoadGame);   
        }

        public override void OnNotificationReceived(Notification notification, NotificationParam param = null)
        {
            switch (notification)
            {
                case Notification.GameLoaded:
                    Debug.Log("Game loaded");
                    break;
            }
        }
    }
}
