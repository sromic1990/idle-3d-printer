﻿using _Idle3DPrinter._Scripts.GameplayRelated.Elements;
using Sourav.Engine.Core.ControllerRelated;
using Sourav.Engine.Core.NotificationRelated;
using Sourav.Engine.Editable.NotificationRelated;
using Sourav.Utilities.Scripts.Components;
using UnityEngine;

namespace _Idle3DPrinter._Scripts.GameplayRelated.Controllers
{
    public class ObjectPrintController : Controller
    {
        [SerializeField] private PrinterMovementElement printerMovementElement;
        [SerializeField] private PrinterHeadMovement printerHeadMovement;
        //SHOULD BE IN UI
        [SerializeField] private Spinner printerHeadSpinner;
        
        public override void OnNotificationReceived(Notification notification, NotificationParam param = null)
        {
            switch (notification)
            {
                case Notification.GameLoaded:
                    SetUpPrinterRevealValue();
                    SetPositionOfPrinter();
                    RevealCurrentObject();
                    SetUpPrinterHeadMovement();
                    break;
                
                case Notification.OneHeadMovementComplete:
                    App.GetLevelData().CurrentPrintedValue += App.GetPrinterData().printerRevealValue;
                    SetPositionOfPrinter();
                    RevealCurrentObject();
//                    App.GetNotificationCenter().Notify(Notification.RevealPrintedObject);

                    if (App.GetLevelData().CurrentPrintedValue > 0.99f)
                    {
                        printerHeadMovement.StopMovement();
                        App.GetNotificationCenter().Notify(Notification.LevelComplete);
                        
                        //SHOULD BE IN UI
                        printerHeadSpinner.StopSpin();
//                        printerHeadSpinner.transform.rotation = Quaternion.identity;
                    }
                    
                    break;
            }
        }

        private void SetUpPrinterRevealValue()
        {
            float steps = App.GetPrintedObjectData().printElements[App.GetLevelData().CurrentStage].numberOfSteps;
            App.GetPrinterData().printerRevealValue = 1 / steps;
        }
        
        private void SetPositionOfPrinter()
        {
            Vector3 startPos = App.GetPrintedObjectData().printElements[App.GetLevelData().CurrentStage]
                .startPrinterPosition.position;
            
            Vector3 endPos = App.GetPrintedObjectData().printElements[App.GetLevelData().CurrentStage]
                .endPrinterPosition.position;
            printerMovementElement.SetUp(startPos, endPos, App.GetLevelData().CurrentPrintedValue);
        }

        private void RevealCurrentObject()
        {
            PrintableObject obj = App.GetPrintedObjectData().printElements[App.GetLevelData().CurrentStage]
                .printableObject;
            
            obj.SetUpPrintableObject();
        }

        private void SetUpPrinterHeadMovement()
        {
            if (App.GetLevelData().CurrentPrintedValue < 0.90f)
            {
                printerHeadMovement.SetUp();
                
                //SHOULD BE IN UI
                printerHeadSpinner.StartSpinning();
            }
            else
            {
                //SHOULD BE IN UI
                printerHeadSpinner.StopSpin();
            }
        }
    }
}
