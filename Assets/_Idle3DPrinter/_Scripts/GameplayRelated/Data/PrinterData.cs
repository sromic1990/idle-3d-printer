﻿using Sirenix.OdinInspector;
using Sourav.Engine.Editable.DataRelated;
using UnityEngine;

namespace _Idle3DPrinter._Scripts.GameplayRelated.Data
{
    public class PrinterData : CommonData
    {
        public Transform printerHead;
        public Vector2 minMaxValue;

        public float incrementNormal;
        public float incrementTapMultiplier;
        public float normalMultiplier;
        public float tapTimerValue;
        [ReadOnly][HideInInspector] public float multiplier;
        [ReadOnly][HideInInspector] public int directionMultiplier;
        [ReadOnly] public float printerRevealValue;
    }
}
