﻿using _Idle3DPrinter._Scripts.GameplayRelated.Elements;
using Sourav.Engine.Editable.DataRelated;
using UnityEngine;

namespace _Idle3DPrinter._Scripts.GameplayRelated.Data
{
    public class PrintedObjectData : CommonData
    {
        public PrintElement[] printElements;
    }
    
    [System.Serializable]
    public class PrintElement
    {
        public PrintableObject printableObject;
        public Transform startPrinterPosition;
        public Transform endPrinterPosition;
        public GameObject objectInRoom;
        public Transform cameraFocusInRoom;
        public float numberOfSteps;
    }
}
