﻿using Sourav.Engine.Core.GameElementRelated;
using UnityEngine;

namespace _Idle3DPrinter._Scripts.GameplayRelated.Elements
{
    [RequireComponent(typeof(Renderer))]
    public class PrintableObject : GameElement
    {
        [SerializeField] private Renderer componentRenderer;

        public void SetUpPrintableObject()
        {
            SetFill(App.GetLevelData().CurrentPrintedValue);
        }

        [Sirenix.OdinInspector.Button()]
        private void ResetFill()
        {
            SetFill(0);
        }

        [Sirenix.OdinInspector.Button()]
        private void SetFill(float fillAmount)
        {
            for (int i = 0; i < componentRenderer.materials.Length; i++)
            {
                componentRenderer.materials[i].SetFloat("_Factor", fillAmount);   
            }
        }
    }
}
