﻿using System.Collections;
using Sirenix.OdinInspector;
using Sourav.Engine.Core.GameElementRelated;
using Sourav.Engine.Editable.NotificationRelated;
using UnityEngine;

namespace _Idle3DPrinter._Scripts.GameplayRelated.Elements
{
    public class PrinterHeadMovement : GameElement
    {
        private float tapTimer;
        private Coroutine _moveHead;

        [Sirenix.OdinInspector.Button()]
        public void SetUp()
        {
            if (_moveHead != null)
            {
                StopCoroutine(_moveHead);
            }

            Vector3 position = App.GetPrinterData().printerHead.localPosition;
            position = new Vector3(App.GetPrinterData().minMaxValue.x, position.y, position.z);
            App.GetPrinterData().printerHead.localPosition = position;
            App.GetPrinterData().directionMultiplier = 1;
            tapTimer = 0;
            
            _moveHead = StartCoroutine(MoveHead());
        }
        
        public void StopMovement()
        {
            if (_moveHead != null)
            {
                StopCoroutine(_moveHead);
            }

            float midPoint = Mathf.Abs((App.GetPrinterData().minMaxValue.y - App.GetPrinterData().minMaxValue.x) / 2.0f);
            midPoint += App.GetPrinterData().minMaxValue.x;
            
            App.GetPrinterData().printerHead.localPosition = new Vector3(midPoint, App.GetPrinterData().printerHead.localPosition.y, App.GetPrinterData().printerHead.localPosition.z);
        }
        
        private IEnumerator MoveHead()
        {
            yield return null;
            while (true)
            {
                float x = App.GetPrinterData().printerHead.localPosition.x;
                
                x = x + (((App.GetPrinterData().incrementNormal) * App.GetPrinterData().directionMultiplier) * App.GetPrinterData().multiplier);
                x = Mathf.Clamp(x, App.GetPrinterData().minMaxValue.x, App.GetPrinterData().minMaxValue.y);

                float difference = (App.GetPrinterData().minMaxValue.y - x) / (App.GetPrinterData().minMaxValue.y - App.GetPrinterData().minMaxValue.x);
                difference = 1 - difference;
                difference = Mathf.Clamp(difference, 0, 1);

                if (difference > 0.98f && App.GetPrinterData().directionMultiplier == 1)
                {
                    App.GetPrinterData().directionMultiplier = -1;
                    App.GetNotificationCenter().Notify(Notification.OneHeadMovementComplete);
                }
                else if (difference < 0.02f && App.GetPrinterData().directionMultiplier == -1)
                {
                    App.GetPrinterData().directionMultiplier = 1;
                    App.GetNotificationCenter().Notify(Notification.OneHeadMovementComplete);
                }
                App.GetPrinterData().printerHead.localPosition = new Vector3(x, App.GetPrinterData().printerHead.localPosition.y, App.GetPrinterData().printerHead.localPosition.z);
                
                tapTimer -= Time.deltaTime;
                tapTimer = Mathf.Max(tapTimer, 0);
                if (tapTimer == 0)
                {
                    App.GetPrinterData().multiplier = App.GetPrinterData().normalMultiplier;
                }
                yield return null;
            }
        }

        [Sirenix.OdinInspector.Button()]
        private void Tap()
        {
            App.GetPrinterData().multiplier = App.GetPrinterData().incrementTapMultiplier;
            tapTimer += App.GetPrinterData().tapTimerValue;
        }
    }
}
