﻿using Sirenix.OdinInspector;
using Sourav.Engine.Core.GameElementRelated;
using UnityEngine;

namespace _Idle3DPrinter._Scripts.GameplayRelated.Elements
{
    public class PrinterMovementElement : GameElement
    {
        [SerializeField] private Transform printer;
        [ReadOnly][SerializeField] private Vector3 startPos;
        [ReadOnly][SerializeField] private Vector3 endPos;

        public void SetUp(Vector3 startPos, Vector3 endPos, float value)
        {
            printer.position = startPos;
            this.startPos = startPos;
            this.endPos = endPos;
            
            SetUpPrinterPosition(value);
        }

        public void SetUpPrinterPosition(float value)
        {
            value = Mathf.Clamp(value, 0, 1);
            Vector3 printerPos = Vector3.Lerp(this.startPos, this.endPos, value);
            printer.position = printerPos;
        }
    }
}
