﻿namespace Sourav.UIPresets.PopUpRelated.Model
{
    public enum PopUpType
    {
        Settings,
        StageComplete,
        LevelComplete,
        GameOver,
    }
}
