﻿using UnityEngine;

namespace Sourav.Engine.Editable.ButtonRelated
{
    //FIXME Fill this up with button types
    public enum ButtonType : int
    {
        Play,
        Settings,
        RestorePurchase,
        
        PopUpClose,
        Store,

        Coin1,
        Coin2,
        Coin3,
        Coin4,
        Coin5,
        Coin6,
        RemoveAds,
        WatchVideoAd,
    }
}
