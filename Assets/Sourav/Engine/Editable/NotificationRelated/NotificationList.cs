﻿namespace Sourav.Engine.Editable.NotificationRelated
{
    //Fill this up with game Notifications  
    
    public enum Notification
    {
        None,
        //Pause Related
        PauseGame,
        ResumeGame,
        GamePaused,
        GameResumed,
        PauseResumeToggle,
        
        //Camera Related
        StartCameraScript,
        StartZoomingCamera,
        StopZoomingCamera,
        StopCameraScript,
        
        //Data Related
        RecordPositionData,
        FetchPositionData,
        PositionData,
        
        //Haptic Related
        HapticHeavy,
        HapticMild,
        HapticVibration,
        
        //Save Load Related
        LoadGame,
        SaveGame,
        GameLoaded,
        DataChanged,

        //UI Related
        ButtonPressed,
        HomeButtonPressed,
        PopUpHidingComplete,
        PopUpShown,
        
        //Toggle Related
        ToggleOn,
        ToggleOff,
        
        //PopUp Related
        ClosePopUp,
        OpenPopUp,
        
        //Purchase Related
        Coins1,
        Coins2,
        Coins3,
        Coins4,
        Coins5,
        Coins6,
        RemoveAds,
        RestorePurchase,
        PurchaseSuccessful,
        ShowPurchaseSuccessful,
        RestorePurchaseSuccessful,
        ShowLoading,
        HideLoading,
        
        //Ad Related
        AdRewarded,
        RewardAdClosed,
        ShowBanner,
        HideBanner,
        ShowInterstitial,
        ShowRewardVideo,
        VideoCoinsRewarded,
        InterstitialClosed,

        //UI Screen Transition related
        ShowScreen,
        TransitionComplete,
        HidePopUpScreen,
        
        //Gameplay Related
        PlayGame,
        LevelComplete,
        OneHeadMovementComplete,
        RevealPrintedObject,


        //UI Related
        SetUI,
        

        //Audio Related
        PlayTapSound,
        PlayOuchSound,
        
        //Input Related
        PointerDown,
        PointerUp,
        
        //Mono Methods Related
        Update,
        FixedUpdate,
        LateUpdate,
        
        //Ticker Related
        SecondTick,
        
        //Offline Timer Related
        OfflineSecondsSet,
        
    }
}



