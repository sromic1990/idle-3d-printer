﻿using UnityEngine;

namespace Sourav.Engine.Editable.DataRelated
{
    public class TimerData : CommonData
    {
        public float waitBeforeDecreasingSpeedometer;
        public float waitBeforeResumingHandMovement;
        public float waitBeforeReactivatingHand;
        public float waitBeforeHidingCurrentOuch;
        public float pauseTimeBeforeHandAnimation;
        public float waitBeforeShowingLevelFailed;
        public float waitTimeForButtonDownAnimation;
        public float waitBeforeReanimatingHand;
        public float waitBeforeShowingStageComplete;
        public float zoomOutAfterGameplay;
        public float waitBeforeCameraSetUp;
        public float waitBeforeNextHaptic;
        public float waitBeforeResettingUi;
    }
}
