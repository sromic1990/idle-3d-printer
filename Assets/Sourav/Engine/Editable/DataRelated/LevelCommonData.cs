﻿using System.Collections.Generic;
using Sourav.Engine.Editable.NotificationRelated;
using UnityEngine;

namespace Sourav.Engine.Editable.DataRelated
{
	//Serialize this class and store in the disk for savegames
	public class LevelCommonData : CommonData
	{
		#region ALMOST FIXED FOR EACH GAME
		[SerializeField] private SaveGame game;

		public int CurrentStage
		{
			get { return game.currentLevel; }
			set
			{
				game.currentLevel = value;
				DataChanged();
			}
		}

		public int CurrentStageActual
		{
			get { return game.currentLevelActual; }
			set
			{
				game.currentLevelActual = value;
				DataChanged();
			}
		}

		public bool AreLevelsExhausted
		{
			get { return game.areLevelsExhausted; }
			set
			{
				game.areLevelsExhausted = value;
				DataChanged();
			}
		}

		public bool IsTutorialOver
		{
			get { return game.isTutorialOver; }
			set
			{
				game.isTutorialOver = value; 
				DataChanged();
			}
		}

		public bool AdsInactive
		{
			get { return game.adsInactive; }
			set
			{
				game.adsInactive = value;
				DataChanged();
			}
		}

		public bool IsMusicOn
		{
			get { return game.isMusicOn; }
			set
			{
				game.isMusicOn = value;
				DataChanged();
			}
		}

		public bool IsSfxOn
		{
			get { return game.isSfxOn; }
			set
			{
				game.isSfxOn = value;
				DataChanged();
			}
		}

		public bool IsVibrationOn
		{
			get { return game.isVibrationOn; }
			set
			{
				game.isVibrationOn = value;
				DataChanged();
			}
		}

		public int AdLastLevel
		{
			get { return game.lastAdLevel; }
			set
			{
				game.lastAdLevel = value;
				DataChanged();
			}
		}
		
		#region TIMER RELATED

		public bool IsOfflineTimerOn
		{
			get { return game.isOfflineTimerOn; }
			set
			{
				game.isOfflineTimerOn = value;
				DataChanged();
			}
		}
		public string LastDateTime
		{
			get { return game.lastDateTime; }
			set
			{
				game.lastDateTime = value;
				DataChanged();
			}
		}
		public int lastDateTimeSeconds;
		public bool isLoaded;
		#endregion
		
		public float CurrentPrintedValue
		{
			get { return game.currentPrintedValue; }
			set
			{
				game.currentPrintedValue = value;
				DataChanged();
			}
		}
		
		public bool isDataChanged;

		[Space(10)] [Header("Defaults")] 
		public int currentLevelDefault;
		public bool isSfxOnDefault;
		public bool isMusicOnDefault;
		public bool isVibrationOnDefault;
		public int coinsDefault;
		public int totalMoneyDefault;
		public int currentMoneyDefault;
		public bool isTutorialOverDefault;
		public bool isIntroVideoOverDefault;
		public bool isAdsInactiveDefault;
		
		#region GAME SPECIFIC DEFAULTS
		public bool isOfflineTimerOnDefault;
		#endregion
		
		private void DataChanged()
		{
			isDataChanged = true;
			App.GetNotificationCenter().Notify(Notification.DataChanged);
			#if UNITY_EDITOR
			App.GetNotificationCenter().Notify(Notification.SaveGame);
			#endif
		}

		public string GetDataInString()
		{
			return JsonUtility.ToJson(game);
		}

		public void SetData(string dataInString)
		{
			game = JsonUtility.FromJson<SaveGame>(dataInString);
		}

		public SaveGame GetCurrentData()
		{
			return game;
		}

		public void LoadData(SaveGame saveGame)
		{
			this.game = saveGame;
		}

		public void SetDefault()
		{
			game = new SaveGame();
			CurrentStage = currentLevelDefault;
			CurrentStageActual = currentLevelDefault;
			IsTutorialOver = isTutorialOverDefault;
			IsSfxOn = isSfxOnDefault;
			IsMusicOn = isMusicOnDefault;
			IsVibrationOn = isVibrationOnDefault;
			AdsInactive = isAdsInactiveDefault;
			AreLevelsExhausted = false;

			IsOfflineTimerOn = isOfflineTimerOnDefault;

		}
		#endregion
	}

	[System.Serializable]
	public class SaveGame
	{
		public int currentLevel;
		public int currentLevelActual;
		public bool areLevelsExhausted;

		public bool isTutorialOver;

		public bool adsInactive;

		public bool isMusicOn;
		public bool isSfxOn;
		public bool isVibrationOn;

		public int lastAdLevel;

		public bool isOfflineTimerOn;
		public string lastDateTime;

		public float currentPrintedValue;
	}
}
