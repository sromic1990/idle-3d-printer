﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sourav.Utilities.Extensions
{
    public static class FloatExtensions
    {
        public static bool Between(this float f, float f_lower, float f_higher)
        {
            float higher = Mathf.Max(f_lower, f_higher);
            float lower = Mathf.Min(f_lower, f_higher);
            
            if (f < higher && f > lower)
                return true;
            else return false;
        }
    }
}
